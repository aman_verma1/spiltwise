from django.shortcuts import render
from .models import Group,Expense,Payment
from django.shortcuts import render, get_object_or_404
from .form import GroupForm,AddExpense
from django.http import HttpResponseRedirect,HttpResponse
from django.core.exceptions import PermissionDenied 
from django.contrib.auth.models import User 
from django.shortcuts import redirect

def home(request):
    groups= Group.objects.all()
    return render(request, 'blog/home.html', {'groups':groups})


def group_detail(request, pk):
    group=get_object_or_404(Group,pk=pk)
    g2=Group.objects.filter(group_name=group.group_name).first()
    g3=Group.objects.filter(group_name=group.group_name).first()
    m2=group.members.all()
    g1=Expense.objects.filter(group__group_name=group.group_name)
    return render(request, 'blog/group_detail.html',{'expense':g1,'member':m2,'pk':pk})

def group_new(request):
    if request.method == "POST":
        form = GroupForm(request.POST)
        if form.is_valid():
            group_name = form.cleaned_data['group_name']
            g1=Group(group_name=group_name)
            g1.save()

            description = form.cleaned_data['description']
            total_amount = form.cleaned_data['total_amount']
            paid_by=form.cleaned_data['Paid_by']
            if not User.objects.filter(username=paid_by): 
                return PermissionDenied
            u1=User.objects.filter(username=paid_by).first()
            g1.members.add(u1) 
            e1=Expense(total_amount=total_amount,Payment=u1,description=description,group=g1)
            e1.save()

            member_name = form.cleaned_data['member_name']
            member_name=member_name.replace('-', ',').split(',')
            for member in member_name:  
                if(member.isdigit()):
                    a1=member   
                else:
                    m1=member
                    if not User.objects.filter(username=m1): 
                        return PermissionDenied
                    member_amount=User.objects.filter(username=m1).first()
                    g1.members.add(member_amount) 
                    continue
            
                p1=Payment(amount=a1,paid=member_amount,expense=e1)
                p1.save()
            return redirect('group-detail',pk=g1.pk)

    else:
        form = GroupForm()
    return render(request, 'blog/group_edit.html', {'form': form})

def expense_detail(request, pk):
    expense=get_object_or_404(Expense,pk=pk)
    e=Payment.objects.filter(expense__description=expense.description)
    return render(request, 'blog/expense_detail.html',{'expense':e})

def expense_new(request,pk):
    if request.method == "POST":
        form = AddExpense(request.POST)
        if form.is_valid():
            group=get_object_or_404(Group,pk=pk)
            g1=Group.objects.filter(group_name=group.group_name).first()
            description = form.cleaned_data['description']
            total_amount = form.cleaned_data['total_amount']
            paid_by=form.cleaned_data['Paid_by']
            if not User.objects.filter(username=paid_by): 
                return PermissionDenied
            u1=User.objects.filter(username=paid_by).first()
            e1=Expense(total_amount=total_amount,Payment=u1,description=description,group=g1)
            e1.save()

            member_name = form.cleaned_data['member_name']
            member_name=member_name.replace('-', ',').split(',')
            for member in member_name:  
                if(member.isdigit()):
                    a1=member   
                else:
                    m1=member
                    if not User.objects.filter(username=m1): 
                        return PermissionDenied
                    member_amount=User.objects.filter(username=m1).first()
                    g1.members.add(member_amount) 
                    continue
            
                p1=Payment(amount=a1,paid=member_amount,expense=e1)
                p1.save()
            return redirect('group-detail',pk=g1.pk)
    else:
        form = AddExpense()
    return render(request, 'blog/add_expense.html', {'form': form})
    



    