from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class Group(models.Model):
    group_name = models.CharField(max_length=100)
    members = models.ManyToManyField(User)

    def __str__(self):
        return self.group_name

class Expense(models.Model):
    total_amount=models.CharField(max_length=100)
    Payment=models.ForeignKey(User, on_delete=models.CASCADE)
    description=models.TextField()
    group=models.ForeignKey(Group, on_delete=models.CASCADE)

class Payment(models.Model):
    amount=models.CharField(max_length=100)
    paid=models.ForeignKey(User, on_delete=models.CASCADE)
    expense=models.ForeignKey(Expense, on_delete=models.CASCADE) 