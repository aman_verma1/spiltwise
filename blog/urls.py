from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='split-home'),
    path('group/<int:pk>/', views.group_detail, name='group-detail'),
    path('group/new/', views.group_new, name='group-new'),
    path('expense/<int:pk>/',views.expense_detail, name='expense-detail'),
    path('group/<int:pk>/new_expense/',views.expense_new, name='expense-new')

]
