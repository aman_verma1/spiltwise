from django import forms

class GroupForm(forms.Form):
    group_name = forms.CharField(label='Your Group name', max_length=100)
    description=forms.CharField(label='Description', max_length=100)
    Paid_by = forms.CharField(label='Paid by', max_length=500)
    total_amount=forms.CharField(label='Amount', max_length=100)
    member_name = forms.CharField(label='Member Names and their amount', max_length=500)

    
class AddExpense(forms.Form):
    description=forms.CharField(label='Description', max_length=100)
    Paid_by = forms.CharField(label='Paid by', max_length=500)
    total_amount=forms.CharField(label='Amount', max_length=100)
    member_name = forms.CharField(label='Member Names and their amount', max_length=500)




    
